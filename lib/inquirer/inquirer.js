const inquirer   = require('inquirer');
const files      = require('../files');
module.exports = {
    askForFramework: () => {
        const questions = [
            {
                type: 'list',
                name: 'Type',
                message: 'Please chose the type of your project :',
                choices: [ 'react', 'angular' ],
                default: 'angular'
            },
        ];
       return inquirer.prompt(questions);

    },
    askForCssType: () => {
      const questions = {
          type: 'list',
          name: 'CssType',
          message: 'Would you like to use scss :',
          choices: ['yes', 'no'],
          default: 'yes'
      }
   return inquirer.prompt(questions);

    },
    askForCssFrameWork: () => {
            const questions = {
                type: 'list',
                name: 'CssTFrame',
                message: 'Would you like to use css framework :',
                choices: ['bootstrap', 'foundation', 'no'],
                default: 'no'
            }
     return inquirer.prompt(questions);

    },
    askForStateMange: () => {
        const questions = {
            type: 'list',
            name: 'stateMange',
            message: 'Would you like to use css stateManagement :',
            choices: ['redux', 'ngrx', 'no'],
            default: 'no'
        }
       return inquirer.prompt(questions);

    },
    askForEffect: () => {
        const questions = {
            type: 'list',
            name: 'effect',
            message: 'Would you like to use side Effect :',
            choices: ['redux-sage', 'redux observable', 'redux thunk' ,'no'],
            default: 'no'
        }
       return inquirer.prompt(questions);

    },
    askForAddition: () => {
        const questions = {
            type: 'checkbox',
            name: 'addition',
            message: 'Would you like to install addition packages  :',
            choices: ['moment', 'lodash' ,'no'],
            default: []
        }
       return inquirer.prompt(questions);

    },
    askForProjectName: async () => {
        const question ={
            type: 'input',
            name: 'projectName',
            message: 'Please enter the project name: ',
            default: 'new-project'
        }
     return inquirer.prompt(question)

    }

}