const fs = require('fs');
const path = require('path');

module.exports = {
    getCurrentDirectoryBase : () => {
        return path.basename(process.cwd());
    },
testfile: () => {
    fs.open('test.ts', 'a', (err, fd) => {
       fd = 3;
        if (err) throw err;
        fs.appendFile(fd, '  test2() {\n' +
            '        console.log(\'test2\');\n' +
            '    }', 'utf8', (err) => {
            fs.close(fd, (err) => {
                if (err) throw err;
            });
            if (err) throw err;
        });
    });
    fs.readFile('test.ts', function (err, data) {
        if (err) {
            return console.error(err);
        }
        console.log("Asynchronous read: " + data.toString());
    });
},
    directoryExists : (filePath) => {
        try {
            return fs.statSync(filePath).isDirectory();
        } catch (err) {
            return false;
        }
    }
};